import uglify from 'rollup-plugin-uglify'

export default {
  entry: 'source/main.js',
  format: 'iife',
  dest: 'public/assets/main.js',
  sourceMap: true,
  moduleName: 'HN',
  plugins: [
    uglify()
  ]
}
