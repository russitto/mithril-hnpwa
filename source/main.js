var log = console.log.bind(console) // eslint-disable-line no-console
var sel = document.querySelector.bind(document)
// var all = document.querySelectorAll.bind(document)

import Controllers from './Controllers'
import ListView from './ListView'
var ctrl = Controllers(m, ListView)

var root = sel('#container')
m.route.prefix('')

log('HN Start!')
headerLinks()
m.route(root, '/', {
  '/': ctrl.Top,
  '/top/:page': ctrl.Top,
  '/new/:page': ctrl.New,
  '/ask/:page': ctrl.Ask,
  '/sho/:page': ctrl.Show,
  '/job/:page': ctrl.Jobs
})

function headerLinks() {
  var header = sel('header.navigation')
  var anchors = header.querySelectorAll('li a')

  header.addEventListener('click', function (ev) {
    if (ev.target.tagName.toLowerCase() == 'a') {
      ev.preventDefault()
      toggleActive(anchors, this)
      m.route.set(ev.target.pathname)
    }
  })
}

function toggleActive(anchors, selected) {
  for (var i = 0, l = anchors.length; i < l; i++) {
    var clas = anchors[i].className.trim()
    anchors[i].className = clas.replace(' selected', '')
    if (anchors[i].href == selected.href) {
      anchors[i].className = clas + ' selected'
    }
  }
}
