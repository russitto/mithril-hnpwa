var log = console.log.bind(console) // eslint-disable-line no-console
var apiBase = '/api/__type__/__page__'
// var apiBase = 'https://api.hackerwebapp.com/__type__?page=__page__'

var runs = 0
export default function(m, listView) {
  var state = {
    list: [],
    loadList: function () {
      state.list = []
      var path = m.route.get()
      var type = 'news'
      if (path.indexOf('/new/') == 0) {
        type = 'newest'
      }
      if (path.indexOf('/ask/') == 0) {
        type = 'ask'
      }
      if (path.indexOf('/sho/') == 0) {
        type = 'show'
      }
      if (path.indexOf('/job/') == 0) {
        type = 'jobs'
      }
      var page = m.route.param('page') || 1
      if (!runs) {
        return
      }
      var url = apiBase.replace('__type__', type).replace('__page__', page)
      m.request(url)
        .then(function (data) {
          state.list = data
        })
    }
  }

  function view () {
    if (!runs) {
      runs = 1
      return listView(m, window.HNData)
    }
    if (state.list.length) {
      return listView(m, state.list)
    }
    return m('.loading')
  }
  var Top  = { oninit: state.loadList, view: view }
  var New  = { oninit: state.loadList, view: view }
  var Ask  = { oninit: state.loadList, view: view }
  var Show = { oninit: state.loadList, view: view }
  var Jobs = { oninit: state.loadList, view: view }

  var Hello = {
    view: function() {
      return m('div.loading')
    }
  }

  return { 
    Top: Top,
    New: New,
    Ask: Ask,
    Show: Show,
    Jobs: Jobs,
    Hello: Hello
  }
}
