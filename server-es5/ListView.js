'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (m, items) {
  var ret = [];
  for (var i = 0, l = items.length; i < l; i++) {
    ret.push(getItem(m, items[i]));
  }
  return ret;
};

function getItem(m, item) {
  var user = '';
  var luser = '';
  var title = item.title;

  if (item.user) {
    user = item.user;
    luser = 'by ' + user;
  }
  if (item.domain) {
    title = '(' + item.domain + ') ' + title;
  }
  var url = item.url;
  var hnBase = 'https://news.ycombinator.com/';
  if (url.indexOf('http') != 0) {
    url = hnBase + url;
  }
  return m('.content', { class: 'content' }, [m('.points', item.points || '…'), m('.info', [m('.title', m('a[rel=noopener][target=_blank][href=' + url + ']', title)), m('.stats', [m('span.user', m('a[rel=noopener][target=_blank][href=' + hnBase + 'user?id=' + user + ']', luser)), m('span.item', item.time_ago), m('span.comments', m('a[rel=noopener][target=_blank][href=' + hnBase + '/item?id=' + item.id + ']', item.comments_count + ' comments'))])])]);
}