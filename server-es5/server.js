'use strict';

var _ListView = require('./ListView');

var _ListView2 = _interopRequireDefault(_ListView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var http = require('http');
var fs = require('fs');
var connect = require('connect');
var compression = require('compression');
var bodyParser = require('body-parser');
var request = require('request');
var pug = require('pug');
var m = require('mithril/render/hyperscript');
var render = require('mithril-node-render');

var log = console.log.bind(console); // eslint-disable-line no-console
var port = process.env.PORT || 8080;
var app = connect();

// gzip/deflate outgoing responses 
app.use(compression());
app.use(bodyParser.json());

var apiBase = 'https://api.hackerwebapp.com/__type__?page=__page__';
app.use('/api', function (req, res) {
  log('api');
  var params = req.url.substr(1).split('/');
  var type = params[0] || 'news';
  var page = params[1] || 1;
  var url = apiBase.replace('__type__', type).replace('__page__', page);
  log(req.url, params, url);
  request(url, function (req2, res2) {
    var contentType = res2.headers['content-type'] || 'application/json';
    var cachCtrl = res2.headers['cache-control'] || 'public, max-age=1800';
    res.writeHead(res2.statusCode, {
      'content-type': contentType,
      'cache-control': cachCtrl
    });
    res.end(res2.body, 'utf-8');
  });
});

// Static:
app.use('/assets/', function (req, res) {
  log('static');
  if (req.url.indexOf('.') == -1) {
    req.url = '/index.html';
  }
  var ext = req.url.replace(/.*\./, '');
  var filePath = 'public/assets/' + req.url;
  var contentType = 'text/html';
  switch (ext) {
    case 'js':
      contentType = 'text/javascript';
      break;
    case 'css':
      contentType = 'text/css';
      break;
    case 'json':
      contentType = 'application/json';
      break;
    case 'gif':
      contentType = 'image/gif';
      break;
    case 'png':
      contentType = 'image/png';
      break;
    case 'jpeg':
    case 'jpg':
      contentType = 'image/jpg';
      break;
    case 'map':
      // contentType = 'application/json'
      contentType = 'application/octet-stream';
      break;
    case 'wav':
      contentType = 'audio/wav';
      break;
  }
  fs.readFile(filePath, function (error, content) {
    if (error) {
      if (error.code == 'ENOENT') {
        fs.readFile('./404.html', function (error, content) {
          res.writeHead(200, { 'Content-Type': contentType });
          res.end(content, 'utf-8');
        });
      } else {
        res.writeHead(500);
        res.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n');
        res.end();
      }
    } else {
      res.writeHead(200, { 'Content-Type': contentType });
      res.end(content, 'utf-8');
    }
  });
});

var compiledTpl = pug.compileFile(__dirname + '/../source/server.pug');
// Render:
app.use(function (req, res) {
  log('render', req.url);
  var params = req.url.substr(1).split('/');
  var type = params[0] || 'news';
  var page = params[1] || 1;
  switch (type) {
    case 'new':
      {
        type = 'newest';
        break;
      }
    case 'sho':
      {
        type = 'show';
        break;
      }
    case 'job':
      {
        type = 'jobs';
        break;
      }
    case 'ask':
      {
        type = 'ask';
        break;
      }
    default:
      {
        type = 'news';
      }
  }
  var url = apiBase.replace('__type__', type).replace('__page__', page);
  request(url, function (req2, res2) {
    render((0, _ListView2.default)(m, JSON.parse(res2.body))).then(function (html) {
      html += '<script>HNData=' + res2.body + '</script>';
      var out = compiledTpl({ active: type, data: html });
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(out, 'utf-8');
    });
  });
});

log('Starting at ' + port);
http.createServer(app).listen(port);